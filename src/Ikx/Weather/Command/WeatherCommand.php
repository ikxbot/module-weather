<?php
namespace Ikx\Weather\Command;

use Ikx\Core\Command\AbstractCommand;
use Ikx\Core\Command\CommandInterface;
use Ikx\Core\Utils\Format;
use Ikx\Core\Utils\MessagingTrait;

class WeatherCommand extends AbstractCommand implements CommandInterface {
    use MessagingTrait;

    public function describe()
    {
        return "Weather information command";
    }

    public function run()
    {
        if (!isset($this->params[0])) {
            $this->msg($this->channel, sprintf('%s: %s command expect at least one parameter, none given',
                Format::bold('ERROR'), $this->command));
        } else {
            $originalTimezone = date_default_timezone_get();
            date_default_timezone_set('Europe/Amsterdam');

            $city = implode(' ', $this->params);
            $url = "https://api.openweathermap.org/data/2.5/weather";
            $query = [
                'q'     => $city,
                'appid' => '3f79d1db614623009140b7be6fe43334',
                'units' => 'metric'
            ];

            $url .= '?' . http_build_query($query);
            $contents = @file_get_contents($url);

            if ($contents) {
                $json = json_decode($contents);

                if ($json) {
                    ini_set('date.timezone', 'Europe/Amsterdam');
                    date_default_timezone_set('Europe/Amsterdam');

                    $this->msg($this->channel, Format::bold("Weather in " . $json->name . ": ") . $json->main->temp . "°C, " . $json->weather[0]->description );
                    $this->msg($this->channel, "Wind: " . $json->wind->speed . " km/h - Visibility: " . $json->visibility . "m");
                    $this->msg($this->channel, "Sunrise: " . strftime('%H:%M', $json->sys->sunrise) . " - Sunset: " . strftime('%H:%M', $json->sys->sunset));
                }
            } else {
                $this->msg($this->channel, "Weather for " . $city . " could not be found.");
            }

        }
    }
}